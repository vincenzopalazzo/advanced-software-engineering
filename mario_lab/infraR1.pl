% service(ServiceId, RequiredHW, MaxRequestRate, MaxLatencyToClient).
service(videoBroadcast, 4, 10, 25).
serviceInstance(s1, videoBroadcast, n42).

%%% R = 1
node(n42, 10, [n39,n41]).
node(n41, 0, [n38,n40]).
node(n39, 5, [n40]).

%requests(ToService, ReversePath, Rate, Latency)
requests(s1, [n41], 50, 0).
requests(s1, [n41],  10, 15).
requests(s1, [n41],  1, 16).

