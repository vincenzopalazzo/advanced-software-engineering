:-consult('infraR2').

operation(undeploy,Si,self) :-
    \+ requests(Si,_,_,_).

operation(migrate,Si,M) :-
    findall((H,R,L),requests(Si,[H|_],R,L),Requests), 
    serviceInstance(Si, S, NodeS), service(S,RequiredHW,MaxRequestRate,_),
    sumRequestRates(Requests,TotalRequestRate), TotalRequestRate>MaxRequestRate,      %(1a)
    %
    findall(K,member((K,_,_),Requests),Ms), sort(Ms,[M]), dif(M,NodeS),
    node(M,AvailableHW, V),
    (
        AvailableHW>=RequiredHW;
    (
        requests(Si,[Q|Qs],_,_),
        findNewNode(Qs,NodeS,RequiredHW)
    )
    ).
    

findNewNode([P|Ps],Y,ReqHW):-
    (node(P, AvailableHWNew, _), 
    dif(P,Y),
    AvailableHWNew>=ReqHW);
    findNewNode(Ps,Y,ReqHW).


operation(replicate,Si,M) :-  
    %prendi tutte le richieste al servizio fatte dal nodo H
    findall((H,R,L),requests(Si,[H|_],R,L),Requests), 
    %prendi il servizio
    serviceInstance(Si, S, N), service(S,RequiredHW,MaxRequestRate,_),
    sumRequestRates(Requests,TotalRequestRate), TotalRequestRate>MaxRequestRate,      %(1a)
    mostRequestsFrom(Requests, M),
      (
       (dif(M,N), node(M,MHW,_), MHW >= RequiredHW)
       ;
       (M=N,node(N,AvailableHW,_),AvailableHW>=RequiredHW)
       ;
        (dif(M,N), node(M,AHW,V), requests(Si,[Q|Qs],_,_), findNewNode(Qs,N,RequiredHW))    
      ).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% "libray" of auxiliary predicates

sumRequestRates([],0).
sumRequestRates([(_,R,_)|Xs], Tot) :- sumRequestRates(Xs,TotXs), Tot is R+TotXs.

mostRequestsFrom(Requests,M) :-
    msort(Requests,OrderedRequests),
    aggregateRequests(OrderedRequests,AggregatedRequests),
    mostRequestsFrom2(AggregatedRequests,(M,_)).

aggregateRequests([],[]).
aggregateRequests([(H,R,_)|Xs],AggregatedRequests) :- aggregateRequests2((H,R),Xs,AggregatedRequests).

aggregateRequests2((H,R),[(H,R1,_)|Xs],AggregatedRequests) :- NewR is R+R1, aggregateRequests2((H,NewR),Xs,AggregatedRequests).
aggregateRequests2((H,R),[(H1,R1,_)|Xs],[(H,R)|AggregatedRequests]) :- dif(H,H1), aggregateRequests2((H1,R1),Xs,AggregatedRequests).
aggregateRequests2((H,R),[],[(H,R)]).

mostRequestsFrom2([X],X).
mostRequestsFrom2([(Mx,Rx)|Xs],(M,R)) :-
    length(Xs,XsLength), XsLength>0, mostRequestsFrom2(Xs,(MXs,RXs)),
    ( (Rx>RXs, M=Mx, R=Rx); (Rx=<RXs, M=MXs, R=RXs) ).