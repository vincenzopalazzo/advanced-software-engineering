:-consult('infraR2').

operation(undeploy,Si,self) :-
    \+ requests(Si,_,_,_).

operation(migrate,Si,M) :-
    findall((H,R,L),requests(Si,[H|_],R,L),Requests), 
    serviceInstance(Si, S, _), service(S,RequiredHW,MaxRequestRate,_),
    sumRequestRates(Requests,TotalRequestRate), TotalRequestRate>MaxRequestRate,      %(1a)
    %
    findall(K,member((K,_,_),Requests),Ms), sort(Ms,[M]), dif(M,self),
    node(M,AvailableHW,_),AvailableHW>=RequiredHW. 

operation(replicate,Si,M) :-  
    findall((H,R,L),requests(Si,[H|_],R,L),Requests), 
    serviceInstance(Si, S, N), service(S,RequiredHW,MaxRequestRate,_),
    sumRequestRates(Requests,TotalRequestRate), TotalRequestRate>MaxRequestRate,      %(1a)
    mostRequestsFrom(Requests, M),
      (
       (dif(M,self), node(M,MHW,_), MHW >= RequiredHW)
       ;
       (M=self,node(N,AvailableHW,_),AvailableHW>=RequiredHW)
      ).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% "libray" of auxiliary predicates

sumRequestRates([],0).
sumRequestRates([(_,R,_)|Xs], Tot) :- sumRequestRates(Xs,TotXs), Tot is R+TotXs.

mostRequestsFrom(Requests,M) :-
    msort(Requests,OrderedRequests),
    aggregateRequests(OrderedRequests,AggregatedRequests),
    mostRequestsFrom2(AggregatedRequests,(M,_)).

aggregateRequests([],[]).
aggregateRequests([(H,R,_)|Xs],AggregatedRequests) :- aggregateRequests2((H,R),Xs,AggregatedRequests).

aggregateRequests2((H,R),[(H,R1,_)|Xs],AggregatedRequests) :- NewR is R+R1, aggregateRequests2((H,NewR),Xs,AggregatedRequests).
aggregateRequests2((H,R),[(H1,R1,_)|Xs],[(H,R)|AggregatedRequests]) :- dif(H,H1), aggregateRequests2((H1,R1),Xs,AggregatedRequests).
aggregateRequests2((H,R),[],[(H,R)]).

mostRequestsFrom2([X],X).
mostRequestsFrom2([(Mx,Rx)|Xs],(M,R)) :-
    length(Xs,XsLength), XsLength>0, mostRequestsFrom2(Xs,(MXs,RXs)),
    ( (Rx>RXs, M=Mx, R=Rx); (Rx=<RXs, M=MXs, R=RXs) ).
