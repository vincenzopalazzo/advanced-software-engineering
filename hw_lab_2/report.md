  -------------- ------------------------------
  Instructors:     Antonio Brogi, Stefano Forti
  -------------- ------------------------------

Introduction
============

The microservices application[@sockshop:github] is analyzed by the
software[@miscrofreshner:github], that contains the microservices
manifest of [@sockshop:github] inside the collections of examples
(Figure\[[1](#fig:start_architecture){reference-type="ref"
reference="fig:start_architecture"}\]). In the
Figure\[[1](#fig:start_architecture){reference-type="ref"
reference="fig:start_architecture"}\] is possible see also all the
smells founded by [@miscrofreshner:github] analysis.

![Start Architecture of system
[@sockshop:github].](Selection_071){#fig:start_architecture}

With the support of [@miscrofreshner:github] that make the analysis of
the microservices application more easy to do without have a good
knowledge of the source code of [@sockshop:github], is possible analyzed
the result of the analysis, and the list of smells found are reported
below.

-   *Endpoint Based Service Interation*: It is an architectural smell
    that violate the horizontal scalability design principle, and an
    application contains this type of smells when one or more
    microservices invokes a specific instance of another microservices;

-   *Wobbly Service Interaction*: It is an architectural smell that is
    violate the isolation of failure design principle, and an
    application contains this type of smell smell when there are two or
    more microservices that depend each other, and this make the
    communication between microservices *wobbly* because some
    interruption of service on one of the microservices can make
    problems for each others;

The architectural smells inside the [@sockshop:github] are eight and the
frequency of this smells are reported in Figure
[\[fig:piechart\]](#fig:piechart){reference-type="ref"
reference="fig:piechart"}.

Possible Solution
=================

The software [@miscrofreshner:github] suggest different solution for
each smell found in application [@sockshop:github], and the solution for
the smell inside [@sockshop:github] are made by suggestions described
inside the article [@Neri2020]. In particular, the solution chased
inside this document are the most commons solution for each smell in
according with [@sockshop:github].

::: {.exmp}
**Example 1**. From the
Figure\[[1](#fig:start_architecture){reference-type="ref"
reference="fig:start_architecture"}\] is possible see that the *front
end microservices* has a smell like *Wobbly Service Interaction*. In
this case, the solution proposed by [@miscrofreshner:github] are
rappresented by the
Figure\[[\[fig:smell_fix_ex\]](#fig:smell_fix_ex){reference-type="ref"
reference="fig:smell_fix_ex"}\].\
A possible solution of this smell can be to add a *Circuit breakerhe*
between the *front end microservices* and all the microservices that the
*front end microservices* can communicate. In this cases are:

-   Order microservice;

-   User microservice;

-   Carts microservice;

-   Catalogue microservice.

The numbers of smells found by [@miscrofreshner:github] are seven now,
and the status of [@miscrofreshner:github] is illustrated in the
Figure\[[\[fig:smell_fix_ex_sol\]](#fig:smell_fix_ex_sol){reference-type="ref"
reference="fig:smell_fix_ex_sol"}\].
[\[ex:example_ui_ref\]]{#ex:example_ui_ref label="ex:example_ui_ref"}
:::

This document adopted the same solution for all *Wobbly Service
Interaction* smells cached inside [@sockshop:github] by
[@miscrofreshner:github], and all the steps are illustrated step by step
in the examples
[\[ex:example_ui_ref\]](#ex:example_ui_ref){reference-type="ref"
reference="ex:example_ui_ref"} and
[\[ex:example_order_ref\]](#ex:example_order_ref){reference-type="ref"
reference="ex:example_order_ref"}.

::: {.exmp}
**Example 2**. In this example is discussed the solution of *Wobbly
Service Interaction* smell inside the Order microservice. As mention
before in this document, the refactoring adopted is to add a *Circuit
breakerhe* between the Order microservice and all the microservices that
it depend. In this cases the refactoring is made between the Order
microservice and all the microservice below:

-   Payment microservice;

-   User microservice;

-   Shipping microservice;

-   Carts microservice;

-   Catalogue microservice.

The refactoring look like as the
Figure\[[2](#fig:order_refactoring){reference-type="ref"
reference="fig:order_refactoring"}\]
[\[ex:example_order_ref\]]{#ex:example_order_ref
label="ex:example_order_ref"}
:::

![Status of [@miscrofreshner:github] analysis after refactoring describe
inside the
Example\[[\[ex:example_order_ref\]](#ex:example_order_ref){reference-type="ref"
reference="ex:example_order_ref"}\].](Selection_079){#fig:order_refactoring}

Another architectural smell common in [@sockshop:github] is *Endpoint
Based Service Interaction*, and for the rest of the section are analyzed
the possible solution to resolve this type of smell.\
The most common solution suggested in [@Neri2020] is to add a *service
discovery mechanism*, the idea of this mechanism is to implement a
service that can be able to register all location of all instances of
microservices in an application.

::: {.exmp}
**Example 3**. In this example is discussed the solution for the
architectural smells *Wobbly Service Interaction*. In particular, as
mentioned before the solution adopted in this document is to add a
*service discovery mechanism* in according with [@Neri2020]. The
solution proposed to modify the code base of each microservice that have
the *Wobbly Service Interaction* architectural smell, that in this cases
are:

-   Catalogue microservice;

-   Carts microservice;

-   Shipping microservice;

-   User microservice;

-   Payment microservice;

-   Order microservice.

After these refactoring to each microservice, is possible observer with
[@miscrofreshner:github] that the application [@sockshop:github] has't
other architectural smells, the final analysis is illustrate inside the
Figure\[[3](#fig:final_ex){reference-type="ref"
reference="fig:final_ex"}\] [\[ex:final_example\]]{#ex:final_example
label="ex:final_example"}
:::

![Status of [@miscrofreshner:github] analysis after refactoring
described inside the
Example\[[\[ex:final_example\]](#ex:final_example){reference-type="ref"
reference="ex:final_example"}\].](Selection_080){#fig:final_ex}

Conclusion
==========

Somethings about conclusion
