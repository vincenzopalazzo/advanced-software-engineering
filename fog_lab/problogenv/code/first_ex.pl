%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% FIRST EXAMPLE %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% OpA si the operator Application
%%% A is the Application
%%% L is a list of component
%%% D us a secure deployment

secFog(OpA, A, D) :-
    app(A, L),
    deployment(OpA, L, D).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%% Deployment %%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% take in input a operator application (OpA)
% the list of component [C | Cs]

% Empty deployment
deployment(_, [], []).

% not empty deployment
deployment(OpA, [C|Cs], [d(C, N, OpN) | D]) :-
    node(N, OpN),
    deployment(OpA, Cs, D).

%% define the list of nodes to deploy
node(fog1, fogOp1).
node(fog2, fogOp2).
node(cloud1, cloudOp).

query(deployment(_,[iot_controller, data_storage, dashboard], _)).

