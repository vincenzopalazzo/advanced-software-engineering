package io.vincenzopalazzo.foglab

import fogtorch.application.Application
import fogtorch.application.ExactThing
import fogtorch.application.ThingRequirement
import fogtorch.deployment.Deployment
import fogtorch.deployment.Search
import fogtorch.infrastructure.Infrastructure
import fogtorch.utils.Couple
import fogtorch.utils.Hardware
import fogtorch.utils.QoSProfile
import java.io.IOException
import java.io.PrintWriter
import java.util.*


class App(val infrastructure: Infrastructure, val application: Application) {
    val random = Random()

    fun initFogTorchApp(): App{
        this.infrastructure.addCloudDatacentre("clou1", listOf("linux", "php", "mySql", "python"), 52.195097, 3.0364791)
        this.infrastructure.addFogNode("fog1", listOf("linux", "php", "mySql", "python"), Hardware(2, 2.0, 32), 43.740186, 10.364619)

        val fogToCloudDownload = samplingFunction(0.98, QoSProfile(40, 10.5), QoSProfile(Int.MAX_VALUE, 0.0))
        val fogToCloudUpload = samplingFunction(0.98, QoSProfile(40, 4.5), QoSProfile(Int.MAX_VALUE, 0.0))
        this.infrastructure.addLink("fog1", "cloud1", fogToCloudDownload, fogToCloudUpload)
        this.infrastructure.addLink("fog1", "cloud2", fogToCloudDownload, fogToCloudUpload)

        this.infrastructure.addThing("water0", "water", 43.7464449, 10.4615923, "fog1");
        this.infrastructure.addThing("video0", "video", 43.7464449, 10.4615923, "fog1");
        this.infrastructure.addThing("moisture0", "moisture", 43.7464449, 10.4615923, "fog1");

        val neededThings = ArrayList<ThingRequirement>()
        neededThings.add(ExactThing("moisture0", QoSProfile(500, 0.1), QoSProfile(500, 0.1)))
        neededThings.add(ExactThing("temperature0", QoSProfile(65, 0.1), QoSProfile(65, 0.1)))

        this.application.addComponent("A", listOf("linux"), Hardware(1, 1.2, 8), neededThings)
        this.application.addComponent("B", listOf("linux", "mySQL"), Hardware(1, 1.2, 8))
        this.application.addComponent("C", listOf("linux", "php"), Hardware(2, 0.7, 4))

        this.application.addLink("A", "B", 160, 0.5, 3.5)
        this.application.addLink("A", "C", 140, 0.4, 1.1)
        this.application.addLink("B", "C", 100, 0.8, 1.0)
        return this
    }

    fun queryStandard(): List<Deployment>{
        val search = Search(this.application, this.infrastructure)
        search.addBusinessPolicies("C", listOf("cloud2", "cloud1"))
        search.findDeployments(true)
        return search.D
    }

    fun samplingFunction(probability: Double, q1: QoSProfile, q2: QoSProfile): QoSProfile{
        val rand = random.nextDouble()
        if (probability == 1.0) {
            return q1;
        }
        if (rand < probability) {
            return q1
        }
        return q2
    }
}

fun main(args: Array<String>) {
    val infrastructure = Infrastructure()
    val application = Application()
    val deployments = App(infrastructure, application).initFogTorchApp().queryStandard()
    val histogram = HashMap<Deployment, Couple<Double, Double>>()
    var pos: Double = deployments.size.toDouble()
    val size: Double = deployments.size.toDouble()
    for(deployment in deployments){
        deployment.consumedResources = infrastructure.consumedResources(deployment, listOf("fog1"))
        if (histogram.containsKey(deployment)) {
            val newCount: Double = histogram.get(deployment)?.a?.plus(1.0) ?: 0.0
            val newPos: Double = histogram.get(deployment)?.b?.plus((pos / size)) ?: 0.0
            histogram.replace(deployment, Couple(newCount, newPos))
        }else{
            histogram.put(deployment, Couple(1.0, pos / size))
        }
        pos--
    }

    try {
        val writer = PrintWriter("result.cvs", "UTF-8")
        writer.println("Deployment, QoS-assurance, Heuristic Rank, Consumed RAM, Consumed HDD, Sum Hardware")
        for (dep in histogram.keys) {
            writer.println(dep.toString() + ", " + histogram[dep] + "," + dep.consumedResources + ", " + (dep.consumedResources.a + dep.consumedResources.b) / 2)
            println(dep.toString() + ", " + histogram[dep] + ", " + dep.consumedResources + ", " + (dep.consumedResources.a + dep.consumedResources.b) / 2)
        }
        writer.close()
    } catch (e: IOException) {
    }
}
