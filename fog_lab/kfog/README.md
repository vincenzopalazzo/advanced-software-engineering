# Kfrog

This is a simple demo in kotlin to understand how the project [FogTorchPI](https://github.com/di-unipi-socc/FogTorchPI)

PS: All the code inside this demo was token from he github repository [FogTorchPI](https://github.com/di-unipi-socc/FogTorchPI)
only the App architecture is from my ideas.